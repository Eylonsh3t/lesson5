#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include <vector>
#include <iostream>
#include <string>



class Menu
{
public:

	Menu();
	~Menu();
	void printMenu();
	void print0Menu();
	void print1Menu();
	void createCircle();
	void createArrow();
	void createTriangle();
	void createRectangle();

	// more functions..

private: 
	Canvas _canvas;
};

