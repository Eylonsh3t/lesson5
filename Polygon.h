#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	Polygon(const std::string& type, const std::string& name);
	virtual ~Polygon();

	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	virtual void draw(const Canvas& canvas) = 0;
	virtual void move(const Point& other) = 0;
	virtual void clearDraw(const Canvas& canvas) = 0;
	// override functions if need (virtual + pure virtual)

protected:
	std::vector<Point> _points;
};