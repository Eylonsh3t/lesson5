#pragma once
#include <cmath>

class Point
{
	double _x;
	double _y;
public:
	Point(double x, double y);
	void changeXAndY (double x, double y);
	virtual ~Point();
	
	double getX() const;
	double getY() const;

	double distance(const Point& other) const;
};