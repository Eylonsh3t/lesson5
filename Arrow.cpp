#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(type, name)
{
	this->_points.reserve(2);
	_points.push_back(a);
	_points.push_back(b);
}

Arrow::~Arrow()
{
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

double Arrow::getArea() const
{
	return 0.0;
}

double Arrow::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]);
}

void Arrow::move(const Point& other)
{
	double x = this->_points[0].getX() + other.getX();
	double y = this->_points[0].getY() + other.getY();
	this->_points[0].changeXAndY(x, y);
	x = this->_points[1].getX() + other.getX();
	y = this->_points[1].getY() + other.getY();
	this->_points[1].changeXAndY(x, y);
}