#include "Shape.h"
#include <iostream>

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{
}

void Shape::printDetails() const
{
	std::cout << this->getType();
	std::cout << "  ";
	std::cout << this->getName();
	std::cout << "		";
	std::cout << this->getPerimeter();
	std::cout << "  ";
	std::cout << this->getArea();

}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}
