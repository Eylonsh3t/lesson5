#include "Point.h"

Point::Point(double x, double y)
{
	this->changeXAndY(x, y);
}

void Point::changeXAndY(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::~Point()
{
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return double(sqrt(exp((this->getX() - other.getX())) + exp((this->getY() - other.getY()))));
}
