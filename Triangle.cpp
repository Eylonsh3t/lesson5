#include "Triangle.h"



Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	this->_points.reserve(3);
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

Triangle::~Triangle()
{
}

double Triangle::getArea() const
{
	return double(abs((this->_points[0].getX() * (this->_points[1].getY() - this->_points[2].getY()) + this->_points[1].getX() * (this->_points[0].getY() - this->_points[2].getY()) + this->_points[2].getX() * (this->_points[0].getY() - this->_points[1].getY())) / 2));
}

double Triangle::getPerimeter() const
{
	return double(this->_points[0].distance(this->_points[1]) + this->_points[1].distance(this->_points[2]) + this->_points[0].distance(this->_points[2]));
}

void Triangle::move(const Point& other)
{
	double x = this->_points[0].getX() + other.getX();
	double y = this->_points[0].getY() + other.getY();
	this->_points[0].changeXAndY(x, y);
	x = this->_points[1].getX() + other.getX();
	y = this->_points[1].getY() + other.getY();
	this->_points[1].changeXAndY(x, y);
	x = this->_points[2].getX() + other.getX();
	y = this->_points[2].getY() + other.getY();
	this->_points[2].changeXAndY(x, y);
}
