#include "Menu.h"

Menu::Menu() 
{
}

Menu::~Menu()
{
}

void Menu::printMenu()
{
	std::cout << "Enter 0 to add a new shape.\n";
	std::cout << "Enter 1 to modify or get information from a current shape.\n";
	std::cout << "Enter 2 to delete all of the shapes.\n";
	std::cout << "Enter 3 to exit.\n";
}

void Menu::print0Menu()
{
	std::cout << "Enter 0 to add a circle.\n";
	std::cout << "Enter 1 to add an arrow.\n";
	std::cout << "Enter 2 to add a triangle.\n";
	std::cout << "Enter 3 to add a rectangle.\n";
}

void Menu::print1Menu()
{
	std::cout << "Enter 0 to move the shape.\n";
	std::cout << "Enter 1 to get its details.\n";
	std::cout << "Enter 2 to remove the shape.\n";
}

void Menu::createCircle()
{
	double x;
	double y;
	double radius;
	std::string type = "Circle";
	std::string name;
	std::cout << "Please enter X:\n";
	std::cin >> x;
	std::cout << "Please enter Y:\n";
	std::cin >> y;
	std::cout << "Please enter radius:\n";
	std::cin >> radius;
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	const Point& center = Point(x, y);
	/*Circle circle = Circle(center, radius, type, name);
	circle.draw(this->_canvas);*/
}

void Menu::createArrow()
{
	double x;
	double x2;
	double y;
	double y2;
	std::string type = "Arrow";
	std::string name;
	std::cout << "Enter the X of point number: 1\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1\n";
	std::cin >> y;
	std::cout << "Enter the X of point number: 2\n";
	std::cin >> x2;
	std::cout << "Enter the Y of point number: 2\n";
	std::cin >> y2;
	std::cout << "Enter the name of the shape:\n";
	std::cin >> name;
	const Point& a = Point(x, y);
	const Point& b = Point(x2, y2);
	Arrow arrow = Arrow(a, b, type, name);
	arrow.draw(this->_canvas);
}

void Menu::createTriangle()
{
	double x;
	double x2;
	double x3;
	double y;
	double y2;
	double y3;
	std::string type = "Triangle";
	std::string name;
	std::cout << "Enter the X of point number: 1\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1\n";
	std::cin >> y;
	std::cout << "Enter the X of point number: 2\n";
	std::cin >> x2;
	std::cout << "Enter the Y of point number: 2\n";
	std::cin >> y2;
	std::cout << "Enter the X of point number: 1\n";
	std::cin >> x3;
	std::cout << "Enter the Y of point number: 1\n";
	std::cin >> y3;
	std::cout << "Enter the name of the shape:\n";
	std::cin >> name;
	const Point& a = Point(x, y);
	const Point& b = Point(x2, y2);
	const Point& c = Point(x3, y3);
	Triangle triangle = Triangle(a, b, c, type, name);
	triangle.draw(this->_canvas);
}

void Menu::createRectangle()
{
	double x;
	double y;
	double length;
	double width;
	std::string type = "Rectangle";
	std::string name;
	std::cout << "Enter the X of the to left corner:\n";
	std::cin >> x;
	std::cout << "Enter the Y of the to left corner:\n";
	std::cin >> y;
	std::cout << "Please enter the length of the shape:\n";
	std::cin >> length;
	std::cout << "Please enter the width of the shape:\n";
	std::cin >> width;
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	const Point& a = Point(x, y);
	myShapes::Rectangle rectangle = myShapes::Rectangle(a, length, width, type, name);
	rectangle.draw(this->_canvas);
}