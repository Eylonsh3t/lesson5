#include "Circle.h"

/*
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type)
{
	//this->_center = center;
	this->_radius = radius;
}

Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

double Circle::getArea() const
{
	return double(PI * ((this->getRadius()) * (this->getRadius())));
}

double Circle::getPerimeter() const
{
	return double(2 * PI * this->getRadius());
}

void Circle::move(const Point& other)
{
	double x = this->_center.getX() + other.getX();
	double y = this->_center.getY() + other.getY();
	this->_center.changeXAndY(x, y);
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}
*/