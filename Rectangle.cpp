#include "Rectangle.h"



myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	this->_points.reserve(2);
	_points.push_back(a);
	Point b = Point((a.getX() + width), (a.getY() + length));
	_points.push_back(b);
	this->_length = length;
	this->_width = width;
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea() const
{
	return double((this->_length) * (this->_width));
}

double myShapes::Rectangle::getPerimeter() const
{
	return double((this->_length) * 2 + (this->_width) * 2);
}

void myShapes::Rectangle::move(const Point& other)
{
	this->_points.clear();
	this->_points.reserve(2);
	_points.push_back(other);
	Point b = Point((other.getX() + this->_width), (other.getY() + this->_length));
	_points.push_back(b);
}