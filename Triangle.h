#include "Polygon.h"
#include <cstdlib>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);
	virtual ~Triangle();

	double getArea() const;
	double getPerimeter() const;
	void move(const Point& other);
	
	// override functions if need (virtual + pure virtual)
};