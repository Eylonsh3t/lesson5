#include "Menu.h"

void main()
{
	int quit = 0;
	int isShapesEmpty = 0;
	int userChoice;
	Menu menu = Menu();
	do
	{
		menu.printMenu();
		std::cin >> userChoice;
		switch (userChoice)
		{
		case 0:
			isShapesEmpty = 1;
			do
			{
				menu.print0Menu();
				std::cin >> userChoice;
				switch (userChoice)
				{
				case 0:
					menu.createCircle();
					break;
				case 1:
					menu.createArrow();
					break;
				case 2:
					menu.createTriangle();
					break;
				case 3:
					menu.createRectangle();
					break;
				}
			} while (userChoice > 3 || userChoice < 0);
			break;
		case 1:
			if (isShapesEmpty)
			{

			}
			break;
		case 2:
			if (isShapesEmpty)
			{
				isShapesEmpty = 0;
			}
			break;
		case 3:
			quit = 1;
			break;
		}
	} while (quit == 0);
}